package honghong.imagemassenger.proxy;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import honghong.imagemassenger.Model.ChatMessageModel;
import honghong.imagemassenger.Model.FriendListParsingModel;
import honghong.imagemassenger.Model.JoinServerparsingModel;
import honghong.imagemassenger.Model.RequestChatMessageRSP;
import honghong.imagemassenger.Model.SendChatMessageRSP;
import honghong.imagemassenger.common.CommonValues;
import honghong.imagemassenger.util.LogUtil;
import honghong.imagemassenger.util.Utils;

/**
 * Created by jun on 2016-01-13.
 */
public class ParseManager implements CommonValues {
    public ParseManager() {
    }

    public JoinServerparsingModel joinMember(String response) {
        LogUtil.LOGV("ParseManager :: joinMember :: response = " + response);
        JoinServerparsingModel temp;
        Gson gson = new Gson();
        temp = gson.fromJson(response, new TypeToken<JoinServerparsingModel>() {
        }.getType());
        LogUtil.LOGV("ParseManager :: joinMember :: initResponseModel = " + temp.toString());
        return temp;
    }

    public FriendListParsingModel parsingAddFriend(String response) {
        LogUtil.LOGV("ParseManager :: parsingAddFriend :: response = " + response);
        FriendListParsingModel temp;
        Gson gson = new Gson();
        temp = gson.fromJson(response, new TypeToken<FriendListParsingModel>() {
        }.getType());
        LogUtil.LOGV("ParseManager :: parsingAddFriend :: FriendListParsingModel = " + temp.toString());
        return temp;
    }

    public SendChatMessageRSP parseSendChatMessageResponse(String response) {
        LogUtil.LOGE("ParseManager :: parseSendChatMessageResponse :: response = " + response);
        //TODO :: 보낸 후 dialogTime 받아야 함
        return new Gson().fromJson(response, SendChatMessageRSP.class);
    }

    public RequestChatMessageRSP parseAllReceiveMessageResponse(String response) {
        LogUtil.LOGE("ParseManager :: parseAllReceiveMessageResponse :: response = " + response);
        RequestChatMessageRSP rsp = new Gson().fromJson(response, RequestChatMessageRSP.class);
        for (int i = 0; i < rsp.result.size(); i++) {
            ChatMessageModel item = rsp.result.get(i);
            item.parsedTime = Utils.parseChatItemDate(item.dialogTime);
            rsp.result.set(i, item);
        }
        return rsp;
    }


//    public Model parseDeletePublicRequest(String response) {
//        LogUtil.LOGV("ParseManager :: parseDeletePublicRequest() :: response = " + response);
//        return new Gson().fromJson(response, Model.class);
//    }
}
