package honghong.imagemassenger.proxy;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import honghong.imagemassenger.common.CommonValues;
import honghong.imagemassenger.util.LogUtil;

/**
 * Created by jun on 2016-01-13.
 */
public class ServerProxy implements CommonValues, RequestURL {
    private static ServerProxy mInstance;
    private RequestQueue mRequestQueue;
    private Context mContext;

    private ServerProxy(Context context) {
        mContext = context;
        mRequestQueue = Volley.newRequestQueue(context);
    }

    public static ServerProxy getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ServerProxy(context);
        }
        return mInstance;
    }


    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public interface ResponseCallback {
        void onResponse(String response);

        void onError(int status);
    }


    // 가입
    public void joinMember(final ResponseCallback callback, final String telnum, final String username, final String email, final String regid) {
        LogUtil.LOGE("ServerProxy :: joinMember() :: telnum = " + telnum + " // username = " + username + " // email = " + email + " // regid = " + regid);

        StringRequest request = new StringRequest(Request.Method.POST, URL_JOIN_MEMBER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string, HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(JOIN_MEMBER_TELNUM, telnum);
                params.put(JOIN_MEMBER_USERNAME, username);
                params.put(JOIN_MEMBER_EMAIL, email);
                params.put(JOIN_MEMBER_REGID, regid);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    public void requestSendChatMessage(final ResponseCallback callback, final String from, final String to, final String imageid, final String text) {
        LogUtil.LOGE("ServerProxy :: requestSendChatMessage() :: from = " + from + " // to = " + to + " // imageid = " + imageid + " // text = " + text);

        StringRequest request = new StringRequest(Request.Method.POST, URL_SEND_MSG, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string, HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(PARAM_FROM, from);
                params.put(PARAM_TO, to);
                params.put(PARAM_IMAGE_ID, imageid);
                params.put(PARAM_TEXT, text);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    //친구 리스트 가져오는 메소드
    public void addFriendList(final ResponseCallback callback) {
        LogUtil.LOGE("ServerProxy :: addFriendList() ");
        RequestParams params = new RequestParams();
        params.setTargetURL(URL_GET_ALL_MEMBER);

        StringRequest request = new StringRequest(Request.Method.GET, params.buildRequestURL(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    public void requestGetAllReceiveMessage(final ResponseCallback callback, final String to, final String lastid) {
        LogUtil.LOGE("ServerProxy :: requestGetChatMessage() :: to = " + to + " // lastid = " + lastid);

        StringRequest request = new StringRequest(Request.Method.POST, URL_RECEIVE_ALL_MSG, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string, HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(PARAM_LAST_ID, lastid);
                params.put(PARAM_TO, to);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    public void requestTargetRoomMessage(final ResponseCallback callback, final String with, final String myid, final String lastid) {
        LogUtil.LOGE("ServerProxy :: requestTargetRoomMessage() :: with = " + with + " // myid = " + myid + " lastid = " + lastid);

        StringRequest request = new StringRequest(Request.Method.POST, URL_RECEIVE_TARGET_ROOM_MSG, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string, HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(PARAM_LAST_ID, lastid);
                params.put(PARAM_MY_ID, myid);
                params.put(PARAM_WITH, with);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }
}