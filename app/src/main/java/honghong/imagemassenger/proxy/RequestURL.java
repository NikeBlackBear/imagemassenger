package honghong.imagemassenger.proxy;

/**
 * Created by Administrator on 2015-12-25.
 */
public interface RequestURL {
    public static final String URL_JOIN_MEMBER = "http://intelliquant.cafe24.com:9000/photalk/register"; // 회원-가입
    public static final String URL_LEAVE_MEMBER = "http://intelliquant.cafe24.com:9000/photalk/unregister"; // 회원-탈퇴
    public static final String URL_GET_ALL_MEMBER = "http://intelliquant.cafe24.com:9000/photalk/getallid"; // 친구 추가
    public static final String URL_SEND_MSG = "http://intelliquant.cafe24.com:9000/photalk/send"; // 메세지-전송
    public static final String URL_RECEIVE_ALL_MSG = "http://intelliquant.cafe24.com:9000/photalk/receiveall"; // 메세지-수신
    public static final String URL_RECEIVE_TARGET_ROOM_MSG = "http://intelliquant.cafe24.com:9000/photalk/dialog"; // 메세지-수신
}