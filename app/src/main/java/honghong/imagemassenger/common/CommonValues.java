package honghong.imagemassenger.common;

import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import honghong.imagemassenger.R;

/**
 * Created by Administrator on 2015-12-25.
 */
public interface CommonValues {
    //GCM sender ID
    public static final String SENDER_ID = "602361123410"; //Do Not Changed.

    //ServerProxy 파라미터
    public static final String JOIN_MEMBER_TELNUM = "telnum"; //Do Not Changed.
    public static final String JOIN_MEMBER_USERNAME = "username"; //Do Not Changed.
    public static final String JOIN_MEMBER_EMAIL = "email"; //Do Not Changed.
    public static final String JOIN_MEMBER_REGID = "regid"; //Do Not Changed.

    public static final String PARAM_FROM = "from";
    public static final String PARAM_TO = "to";
    public static final String PARAM_IMAGE_ID = "imageid";
    public static final String PARAM_TEXT = "text";
    public static final String PARAM_LAST_ID = "lastid";
    public static final String PARAM_MY_ID = "myid";
    public static final String PARAM_WITH = "withid";

    //프리퍼런스
    public static final String PROPERTY_REG_ID = "registration_id"; //Do Not Changed.
    public static final String PROPERTY_APP_VERSION = "appVersion";
    public static final String USER_ID = "userid";

    public static final String RESPONSE_OK = "0";
    public static final String RESPONSE_ERROR = "1";

    //ImageLoader Option
    public static final DisplayImageOptions IMAGE_LODER_OPTION = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.ic_stub).showImageForEmptyUri(R.drawable.ic_empty)
            .showImageOnFail(R.drawable.ic_error).cacheInMemory(true).cacheOnDisk(true).considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565).build();  // 이미지 로더 기본 세팅
}
