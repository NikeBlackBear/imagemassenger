package honghong.imagemassenger.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.List;
import java.util.Vector;

import honghong.imagemassenger.R;
import honghong.imagemassenger.ViewPager.FriendListFragment;
import honghong.imagemassenger.ViewPager.PageAdapter;
import honghong.imagemassenger.ViewPager.SettingFragment;
import honghong.imagemassenger.ViewPager.ShopFragment;
import honghong.imagemassenger.ViewPager.TalkRoomListFragment;

/**
 * Created by jun on 2015-12-17.
 */
public class MainActivity extends FragmentActivity implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {
    final static String FRIEND_LIST = "t1";
//    final static String TALKROOM_LIST = "t2";
    final static String SHOP = "t3";
    final static String SETTING = "t4";

    private TabHost mTabHost;
    private ViewPager mViewPager;
    private PageAdapter mPagerAdapter;

    private static View createTabView(final Context context, final String text) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        ImageView img;
        TextView name;

        img = (ImageView) view.findViewById(R.id.tabs_image);
        if (text.equals(FRIEND_LIST)) {
            img.setImageResource(R.drawable.tab_friend_list_icon);
        }
//        else if (text.equals(TALKROOM_LIST)) {
//            img.setImageResource(R.drawable.tab_chat_room_list_icon);
//        }
        else if (text.equals(SHOP)) {
            img.setImageResource(R.drawable.tab_shop_icon);
        }else if (text.equals(SETTING)) {
            img.setImageResource(R.drawable.tab_setting_icon);
        }
        return view;
    }

    private static void AddTab(MainActivity activity, TabHost tabHost, TabHost.TabSpec tabSpec, TabInfo tabInfo) {
        // Attach a Tab view factory to the spec
        tabSpec.setContent(activity.new TabFactory(activity));
        tabHost.addTab(tabSpec);
    }

    /**
     * (non-Javadoc)
     *
     * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initialiseTabHost(savedInstanceState);
        if (savedInstanceState != null) {
            mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab")); //set the tab as per the saved state
        }
        this.intialiseViewPager();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mViewPager != null) {
            mViewPager.removeOnPageChangeListener(this);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see android.support.v4.app.FragmentActivity#onSaveInstanceState(android.os.Bundle)
     */
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("tab", mTabHost.getCurrentTabTag()); //save the tab selected
        super.onSaveInstanceState(outState);
    }

    /**
     * Initialise ViewPager
     */
    private void intialiseViewPager() {
        List<Fragment> fragments = new Vector<Fragment>();
        fragments.add(Fragment.instantiate(this, FriendListFragment.class.getName()));
//        fragments.add(Fragment.instantiate(this, TalkRoomListFragment.class.getName()));
        fragments.add(Fragment.instantiate(this, ShopFragment.class.getName()));
        fragments.add(Fragment.instantiate(this, SettingFragment.class.getName()));
        this.mPagerAdapter = new PageAdapter(super.getSupportFragmentManager(), fragments);
        this.mViewPager = (ViewPager) super.findViewById(R.id.viewpager);
        this.mViewPager.setAdapter(this.mPagerAdapter);
        this.mViewPager.addOnPageChangeListener(this);
    }

    /**
     * Initialise the Tab Host
     */
    private void initialiseTabHost(Bundle args) {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();
        MainActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab1").setIndicator(createTabView(MainActivity.this, FRIEND_LIST)),
                (new TabInfo("Tab1", FriendListFragment.class, args)));
//        MainActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab2").setIndicator(createTabView(MainActivity.this, TALKROOM_LIST)),
//                (new TabInfo("Tab2", TalkRoomListFragment.class, args)));
        MainActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab3").setIndicator(createTabView(MainActivity.this, SHOP)),
                (new TabInfo("Tab3", ShopFragment.class, args)));
        MainActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab4").setIndicator(createTabView(MainActivity.this, SETTING)),
                (new TabInfo("Tab4", SettingFragment.class, args)));
        mTabHost.setCurrentTab(0);
        mTabHost.setOnTabChangedListener(this);
    }

    public void onTabChanged(String tag) {
        int pos = this.mTabHost.getCurrentTab();
        this.mViewPager.setCurrentItem(pos);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset,
                               int positionOffsetPixels) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageSelected(int position) {
        // TODO Auto-generated method stub
        this.mTabHost.setCurrentTab(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == ViewPager.SCROLL_STATE_IDLE) {
            int position = mViewPager.getCurrentItem();
            // 원하는 페이지에 맞게 조건
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(mViewPager.getWindowToken(), 0);
        }
    }

    private class TabInfo {
        private String tag;
        private Class<?> clss;
        private Bundle args;
        private Fragment fragment;

        TabInfo(String tag, Class<?> clazz, Bundle args) {
            this.tag = tag;
            this.clss = clazz;
            this.args = args;
        }
    }

    class TabFactory implements TabHost.TabContentFactory {

        private final Context mContext;

        public TabFactory(Context context) {
            mContext = context;
        }

        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }

    }
}