package honghong.imagemassenger.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import honghong.imagemassenger.Model.ChatMessageModel;
import honghong.imagemassenger.Model.RequestChatMessageRSP;
import honghong.imagemassenger.Model.userInfoModel;
import honghong.imagemassenger.R;
import honghong.imagemassenger.adapter.ChatListAdapter;
import honghong.imagemassenger.common.CommonValues;
import honghong.imagemassenger.db.DatabaseManager;
import honghong.imagemassenger.proxy.ParseManager;
import honghong.imagemassenger.proxy.ServerProxy;
import honghong.imagemassenger.util.LogUtil;
import honghong.imagemassenger.util.Utils;
import honghong.imagemassenger.widget.ChatContentObservable;
import honghong.imagemassenger.widget.ChatItemView;
import honghong.imagemassenger.widget.ComposeChatContentView;
import honghong.imagemassenger.widget.FlexibleScrollListener;
import honghong.imagemassenger.widget.IntentListener;
import honghong.imagemassenger.widget.SoftKeyboardDetectorView;
import honghong.imagemassenger.widget.TalkRoomItemDecoration;

/**
 * Created by Administrator on 2016-01-11.
 */
public class TalkRoomActivity extends AppCompatActivity implements CommonValues {
    private final int MODE_IDLE = 0;
    private final int MODE_COMPOSE = 1;

    private Toolbar mTalkRoomToolbar;
    private RecyclerView mTalkRoomRecyclerView;
    private EditText mInputSendMessage;
    private SoftKeyboardDetectorView mContent;
    private ComposeChatContentView mComposeChatContentView;
    private ImageView mSendBtn;

    private ChatListAdapter mChatListAdapter;
    private Animation mDropDownAnim;
    private ChatContentObservable mObservable;
    private List<ChatMessageModel> mChatDataList;
    private userInfoModel mTargetFriendData;
    private InputMethodManager mInputMethodManager;
    private DisplayMetrics mMetrics = new DisplayMetrics();

    private int mMode = MODE_IDLE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_talk_room);
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mComposeChatContentView != null) {
            mComposeChatContentView.unregisterObserver();
        }
    }

    private void init() {
        getWindowManager().getDefaultDisplay().getMetrics(mMetrics);

        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            userInfoModel model = (userInfoModel) intent.getSerializableExtra("UserInfo");
            mTargetFriendData = (userInfoModel) intent.getSerializableExtra("UserInfo");
            LogUtil.LOGE("TalkRoomActivity :: friend data :: id = " + model.getId() + " name = " + model.getUserName());
        }

        if (getUserId().equals("-1") || mTargetFriendData == null) {
            Toast.makeText(this, R.string.str_abnormal_access, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        //채팅메세지 Observable 객체
        mObservable = new ChatContentObservable();

        //채팅작성 화면 애니메이션 객체
        mDropDownAnim = AnimationUtils.loadAnimation(this, R.anim.dropdown);

        //View
        mTalkRoomToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mTalkRoomToolbar);
        setTitle(mTargetFriendData.getUserName());
        mTalkRoomToolbar.setTitleTextColor(Color.WHITE);
        mTalkRoomToolbar.setNavigationIcon(R.drawable.left_arrow);
        mTalkRoomToolbar.setNavigationOnClickListener(mToolbarBackBtnClickListener);

        mSendBtn = (ImageView) findViewById(R.id.send_btn);
        mSendBtn.setOnClickListener(mSendBtnClickListener);

        mComposeChatContentView = (ComposeChatContentView) findViewById(R.id.chat_compose_view);
        mComposeChatContentView.registerObserver(mObservable);

        mInputSendMessage = (EditText) findViewById(R.id.input_send_message);
        mInputSendMessage.addTextChangedListener(mContentTextWatcher);

        mTalkRoomRecyclerView = (RecyclerView) findViewById(R.id.chat_list);
        mTalkRoomRecyclerView.setHasFixedSize(false);
        mTalkRoomRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mTalkRoomRecyclerView.addItemDecoration(new TalkRoomItemDecoration(50, mMetrics.density));
        mTalkRoomRecyclerView.addOnScrollListener(mFlexibleScrollListener);

        mContent = (SoftKeyboardDetectorView) findViewById(R.id.list_parent);
        mContent.setOnKeyboardStateListener(mOnKeyboardVisibleStateListener);

        loadChatData();
    }

    private void loadChatData() {
        mChatDataList = DatabaseManager.getInstance(this).getChatRowData(mTargetFriendData.getId());
        mChatListAdapter = new ChatListAdapter(this, mChatDataList, mTargetFriendData, mTalkRoomRecyclerView);
        mTalkRoomRecyclerView.setAdapter(mChatListAdapter);
        scrollToLast();

        //TODO :: print Test DB
        DatabaseManager.getInstance(this).printChatDB(mTargetFriendData.getId());

        //TODO :: 프로토타입에서 내부DB 삭제 시나리오를 배제 함으로 해당 루틴 임시 제거
        //ServerProxy.getInstance(this).requestTargetRoomMessage(mTargetRoomAllMessageResponseCallback, mTargetFriendData.getId(),
        //       getUserId(), "0");
    }

    private void scrollToLast() {
        if (mChatDataList != null && mChatDataList.size() > 0) {
            mTalkRoomRecyclerView.getLayoutManager().scrollToPosition(mChatDataList.size() - 1);
        }
    }

    private String getUserId() {
        return getSharedPreferences("pref", MODE_PRIVATE).getString(USER_ID, "-1");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtil.LOGE("requestcode = " + requestCode + " resultCode = " + resultCode);
        if (requestCode == ComposeChatContentView.REQUEST_CODE_GALLERY) {
            ((IntentListener) mComposeChatContentView).deliveryActivityResult(requestCode, resultCode, data);
            mInputMethodManager.toggleSoftInput(0, 0);
        }
    }

    private final View.OnClickListener mToolbarBackBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private final View.OnClickListener mSendBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mMode == MODE_IDLE) {
                if (mInputSendMessage.getText().toString().equals("")) {
                    Toast.makeText(TalkRoomActivity.this, R.string.str_alert_no_chat_data, Toast.LENGTH_SHORT).show();
                }
                mInputMethodManager.toggleSoftInput(0, 0);
            } else if (!mInputSendMessage.getText().toString().equals("")) {
                //작성화면에서 입력 된 글과 선택된 이미지에 대한 결과값을 가져온뒤, id 를 설정한다. loading UI 표현을 위해 state 를 변경한다
                ChatMessageModel model = mComposeChatContentView.createComposeData();
                model.toId = mTargetFriendData.getId();
                model.fromId = getUserId();
                model.dialogTime = String.valueOf(System.currentTimeMillis());
                model.parsedTime = Utils.parseChatItemDate(model.dialogTime);
                model.state = ChatItemView.STATE_LOADING;

                //List 에 입력 된 값을 add 하고 리스트 갱신 후 scroll 한다
                mChatDataList.add(model);
                mChatListAdapter.notifyItemInserted(mChatDataList.size() - 1);
                scrollToLast();

                //notifyItemInsert -> scrollToPosition 을 통해 내부적으로 holder가 갱신 될 약간의 시간이 필요함
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((ChatItemView) mTalkRoomRecyclerView.findViewHolderForAdapterPosition(mChatDataList.size() - 1).itemView).send();
                        mInputMethodManager.toggleSoftInput(0, 0);
                    }
                }, 100);

                mInputSendMessage.setText("");
            }
        }
    };

    private final TextWatcher mContentTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mObservable.changeChatMessage(mInputSendMessage.getText().toString());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    //Keypad Visible 상태 Listener
    private final SoftKeyboardDetectorView.OnKeyboardStateListener mOnKeyboardVisibleStateListener = new SoftKeyboardDetectorView.OnKeyboardStateListener() {
        @Override
        public void onShowSoftKeyboard() {
            LogUtil.LOGE("onShowSoftKeyboard() ::" + mComposeChatContentView.getVisibility());
            if (mComposeChatContentView.getVisibility() == View.INVISIBLE) {
                LogUtil.LOGE("onShowSoftKeyboard() ::visible");
                mSendBtn.setImageResource(R.drawable.send_btn);
                mComposeChatContentView.setVisibility(View.VISIBLE);
                mComposeChatContentView.startAnimation(mDropDownAnim);
                mMode = MODE_COMPOSE;
            }
        }

        @Override
        public void onHiddenSoftKeyboard() {
            LogUtil.LOGE("onHiddenSoftKeyboard() ::" + mComposeChatContentView.getVisibility());
            LogUtil.LOGE("onHiddenSoftKeyboard() :: gallery state = " + mComposeChatContentView.getGalleryProcessState());
            if (mComposeChatContentView.getGalleryProcessState() == ComposeChatContentView.GalleryProcessSate.STATE_PICK) {
                mComposeChatContentView.setGalleryProcessState(ComposeChatContentView.GalleryProcessSate.STATE_IDLE);
                return;
            }

            if (mComposeChatContentView.getVisibility() == View.VISIBLE) {
                LogUtil.LOGE("onShowSoftKeyboard() :: gone");
                mSendBtn.setImageResource(R.drawable.compose_btn);
                mComposeChatContentView.setVisibility(View.INVISIBLE);
                mMode = MODE_IDLE;
            }
        }
    };

    private FlexibleScrollListener mFlexibleScrollListener = new FlexibleScrollListener() {
        @Override
        public void onHide() {
            mTalkRoomToolbar.animate().translationY(-mTalkRoomToolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
        }

        @Override
        public void onShow() {
            mTalkRoomToolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
        }
    };
}