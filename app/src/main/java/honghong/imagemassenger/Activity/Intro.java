package honghong.imagemassenger.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import honghong.imagemassenger.R;
import honghong.imagemassenger.common.CommonValues;
import honghong.imagemassenger.util.LogUtil;


public class Intro extends Activity implements CommonValues{
    private GoogleCloudMessaging gcm;
    private Context context;
    private String regid;
    private String userid;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        Handler handler = new Handler() {
            public void handleMessage(Message msg) {
                init();
            }
        };
        handler.sendEmptyMessageDelayed(0, 3000);
    }
    private void init(){
        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        userid = pref.getString(USER_ID, "");
        context = getApplicationContext();

        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");

        if (userid.isEmpty()) {
            LogUtil.LOGE("Intro :: onCreate :: userid is Empty");
            StartGcm();
        } else {
            LogUtil.LOGE("Intro :: onCreate :: userid = " + userid);
            startMain();
        }
    }

    private void startMain(){
        Intent intent = new Intent(Intro.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void startJoin() {
        Intent intent = new Intent(Intro.this, Join.class);
        startActivity(intent);
        finish();
    }

    //{@code PackageManager}의 애플리케이션의 버전 코드
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // 일어나지 않아야 합니다
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void StartGcm() {
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);
            if (regid.isEmpty()) {
                LogUtil.LOGE("Intro :: StartGcm :: Call registerInBackground ");
                registerInBackground();
            } else {
                startJoin();
            }
            LogUtil.LOGE(regid);
        } else {
            LogUtil.LOGE("Intro :: onCreate :: No valid Google Play Services APK found.");
        }
    }

    public boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(Intro.this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, Intro.this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                LogUtil.LOGE("GCMManager :: This device is not supported.");
            }
            return false;
        }
        return true;
    }

    //현재의 registration ID 를 가져옴.
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        LogUtil.LOGE("Intro :: getRegistrationId :: regId = " + registrationId);
        if (registrationId.isEmpty()) {
            LogUtil.LOGE("Intro :: getRegistrationId :: Registration not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            LogUtil.LOGE("Intro :: getRegistrationId :: App version changed.");
            return "";
        }
        return registrationId;
    }

    // @return 애플리케이션의 {@code SharedPreferences}.
    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(Intro.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private void registerInBackground() {
        DeviceGCMRegisterTask asd = new DeviceGCMRegisterTask();
        LogUtil.LOGE("Intro :: StartGcm :: Call GCMID Generater ");
        asd.execute(null, null, null);
    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        LogUtil.LOGE("Saving regId on app version " + appVersion);
        LogUtil.LOGE("Intro :: regId = " + regId);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
        init();
    }

    class DeviceGCMRegisterTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            String msg = "";
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(context);
                }
                regid = gcm.register(SENDER_ID);
                msg = "Device registered, registration ID=" + regid;

                storeRegistrationId(context, regid);
            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
            }
            return msg;
        }
    }
}
