package honghong.imagemassenger.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import honghong.imagemassenger.Model.JoinServerparsingModel;
import honghong.imagemassenger.R;
import honghong.imagemassenger.common.CommonValues;
import honghong.imagemassenger.proxy.ParseManager;
import honghong.imagemassenger.proxy.ServerProxy;
import honghong.imagemassenger.util.LogUtil;
import honghong.imagemassenger.util.Utils;

public class Join extends Activity implements CommonValues {
    private TextView submit_Btn;
    private String phoneNum, regid;
    private EditText emailEdt, userNameEdt;
    private EditText phoneNumEdt; //테스트용

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);
        init();
    }

    private void init() {
        final SharedPreferences prefs = getSharedPreferences(Intro.class.getSimpleName(), Context.MODE_PRIVATE);
        regid = prefs.getString(PROPERTY_REG_ID, "");
        LogUtil.LOGE("Join :: regi id = " + regid);
        submit_Btn = (TextView) findViewById(R.id.submit_Btn);
        submit_Btn.setOnClickListener(submitBtnOnClickListener);
        emailEdt = (EditText) findViewById(R.id.email);
        userNameEdt = (EditText) findViewById(R.id.user_name);
        phoneNumEdt = (EditText)findViewById(R.id.phone_num); // 테스트용
    }

    private View.OnClickListener submitBtnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
//            phoneNum = getPhoneNumber();
            phoneNum = phoneNumEdt.getText().toString();
          ServerProxy.getInstance(Join.this).joinMember(joinCallback, phoneNum, userNameEdt.getText().toString(), emailEdt.getText().toString(),regid);
        }
    };

    private String getPhoneNumber() {
        TelephonyManager mgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        return new Utils().transformTelnum(mgr.getLine1Number());
    }

    private ServerProxy.ResponseCallback joinCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (Join.this != null) {
                LogUtil.LOGV("Join :: joinCallback :: onResponse() :: response = " + response);
                JoinServerparsingModel Modeltemp = new ParseManager().joinMember(response);
                if (Modeltemp.getStatus().equals("0")) {
                    Toast.makeText(Join.this, "성공.", Toast.LENGTH_SHORT).show();
                    SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString(USER_ID, Modeltemp.getId());
                    LogUtil.LOGV("Join :: joinCallback :: preference ");
                    editor.commit();

                    Intent intent = new Intent(Join.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(Join.this, "실패하였습니다.", Toast.LENGTH_SHORT).show();
                }
            } else {
                LogUtil.LOGV("Join :: joinCallback :: not exists next data");
            }
        }
        @Override
        public void onError(int status) {
            LogUtil.LOGV("Join :: onError() :: status =" + status);
        }
    };
}
