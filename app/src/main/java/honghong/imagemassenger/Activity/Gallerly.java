package honghong.imagemassenger.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import honghong.imagemassenger.Model.ImageModel;
import honghong.imagemassenger.R;
import honghong.imagemassenger.adapter.GalleryAdapter;
import honghong.imagemassenger.util.LogUtil;
import honghong.imagemassenger.util.Utils;

public class Gallerly extends AppCompatActivity {
    private GridView gallery;
    private TextView selectBtn;
    private ArrayList<ImageModel> imageList;
    private GalleryAdapter mGalleryAdapter;
    private String resultImgID;
    private int resultImgPosition;
    private Intent intent;
    private boolean CheckedImgFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallerly);
        init();
    }

    private void init() {
        CheckedImgFlag = false;
        resultImgID = "No";
        resultImgPosition = -1;
        gallery = (GridView) findViewById(R.id.gallerypanel);
        selectBtn = (TextView) findViewById(R.id.select_btn);
        selectBtn.setOnClickListener(selectBtnClickListener);
        imageList = new ArrayList<ImageModel>();
        makeImageList();
        mGalleryAdapter = new GalleryAdapter(Gallerly.this, imageList);
        mGalleryAdapter.setOnImageClickListener(ImageClickListener);
        gallery.setAdapter(mGalleryAdapter);
        intent = getIntent();
    }

    private View.OnClickListener selectBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast.makeText(Gallerly.this, "선택완료", Toast.LENGTH_SHORT).show();
            intent.putExtra("resultImgID", resultImgID);
            intent.putExtra("resultImgPosition", resultImgPosition);
            setResult(RESULT_OK, intent);
            finish();
        }
    };


    private GalleryAdapter.onImageClickListener ImageClickListener = new GalleryAdapter.onImageClickListener() {
        @Override
        public void onClick(ImageModel data, int position) {
            LogUtil.LOGE("Gallery :: ImageClickListener :: Click!!");
            if (data.getCheckState()) {
                data.setCheckState(false);
                resultImgID = "No";
                resultImgPosition = -1;
                CheckedImgFlag = false;
                LogUtil.LOGE("Gallery :: ImageClickListener :: UnImgSelect!!");
            } else {
                if (CheckedImgFlag) {
                    Toast.makeText(Gallerly.this, "한 장만 선택 가능합니다.", Toast.LENGTH_SHORT).show();
                } else {
                    data.setCheckState(true);
                    resultImgID = data.getId();
                    resultImgPosition = position;
                    LogUtil.LOGE("Gallery :: ImageClickListener :: ImgSelect!!");
                    CheckedImgFlag = true;
                }
            }
        }
    };

    private void makeImageList() {
//        for (int i = 0; i < 15; i++) {
//            Uri imageUri = Uri.parse("android.resource://" + getPackageName() + "/" + "R.drawable.test_image_0"+String.format("%02d", i));
//            String ImgAdr = "android.resource://" + getPackageName() + "/" + "R.drawable.test_image_0" + String.format("%02d", i);
//            String ImgAdr = "drawable://" + Drawable.createFromPath("R.drawable.test_image_001" + String.format("%02d", i));
//            Drawable ImgAdrr =  this.getResources().getDrawable(R.drawable.test_image_001);
//            String ImgAdr =  "drawable://" + R.drawable.test_image_001;
//            String ImgAdr = "drawable://" + (R.drawable.test_image_001 + i);
//            ImageModel tempModel = new ImageModel();
//            int k = i+1;
//            tempModel.setId("test_image_0" + String.format("%02d", k));
//            tempModel.setAddress(ImgAdr);
//            tempModel.setCheckState(false);
//            imageList.add(tempModel);
//            LogUtil.LOGE("Gallerly :: imageList :: get(" + i + ").getadd = " + imageList.get(i).getAddress());
//        }
        Utils.getChatBackgroundResIds(this, imageList);
    }
}