package honghong.imagemassenger.util;

/**
 * Created by Administrator on 2015-12-25.
 */
public class LogUtil {
    private final static String TAG = "ImageMessenger";
    private final static boolean DEBUG = true;

    public static void LOGV(String msg) {
        if (DEBUG) {
            android.util.Log.v(TAG, msg);
        }
    }

    public static void LOGE(String msg) {
        if (DEBUG) {
            android.util.Log.e(TAG, msg);
        }
    }

    public static void LOGD(String msg) {
        if (DEBUG) {
            android.util.Log.d(TAG, msg);
        }
    }

    public static void LOGI(String msg) {
        if (DEBUG) {
            android.util.Log.i(TAG, msg);
        }
    }
}