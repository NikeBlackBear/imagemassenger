package honghong.imagemassenger.util;

import android.content.Context;

import honghong.imagemassenger.common.CommonValues;

/**
 * Created by Administrator on 2016-02-03.
 */
public class MessengerPreferences implements CommonValues {
    public static String getUserId(Context context) {
        return context.getSharedPreferences("pref", Context.MODE_PRIVATE).getString(USER_ID, "-1");
    }
}
