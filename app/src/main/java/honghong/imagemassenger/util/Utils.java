package honghong.imagemassenger.util;

import android.content.Context;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import honghong.imagemassenger.Model.ImageModel;

/**
 * Created by Administrator on 2016-01-13.
 */
public class Utils {

    private static final String EXCLUSION_QUOTES = "&quot";
    private static final String BACKGROUND_RESOURCE_PREFIX = "@drawable/chat_bg_";
    private static final int NO_VALID_RES_ID = 0;

    public static ArrayList<Integer> getChatBackgroundResIds(Context context) {
        ArrayList<Integer> resIdList = new ArrayList<Integer>();
        int id = -1;
        int count = 0;
        while (true) {
            id = context.getResources().getIdentifier(BACKGROUND_RESOURCE_PREFIX + count, "drawable", context.getPackageName());
            if (id == NO_VALID_RES_ID) {
                break;
            } else {
                count++;
                resIdList.add(id);
            }
        }
        LogUtil.LOGE("getChatBackgroundResIds :: size = " + resIdList.size());
        for (int i = 0; i < resIdList.size(); i++) {
            LogUtil.LOGE("i = " + i + " id = " + resIdList.get(i) + " + name = " + getResourceName(context, resIdList.get(i)));
        }
        //Collections.shuffle(resIdList);
        return resIdList;
    }

    public static void getChatBackgroundResIds(Context context, ArrayList<ImageModel> imgList) {
        int id = -1;
        int count = 0;
        while (true) {
            id = context.getResources().getIdentifier(BACKGROUND_RESOURCE_PREFIX + count, "drawable", context.getPackageName());
            if (id == NO_VALID_RES_ID) {
                break;
            } else {
                ImageModel tempModel = new ImageModel();
                tempModel.setId("chat_bg_" + count);
                tempModel.setAddress("drawable://" + id);
                tempModel.setCheckState(false);
                imgList.add(tempModel);
                count++;
            }
        }
    }

    public static int getResourceId(Context context, String resName) {
        return context.getResources().getIdentifier("@drawable/" + resName, "drawable", context.getPackageName());
    }

    public static String getResourceName(Context context, int resId) {
        return context.getResources().getResourceEntryName(resId);
    }

    public static String encodeUTF8(String targetText) {
        try {
            if (targetText.contains("\"")) {
                targetText = targetText.replaceAll("\"", EXCLUSION_QUOTES);
            }
            targetText = URLEncoder.encode(targetText, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return targetText;
    }

    // 전화번호 형식 통일
    public String transformTelnum(String telNum) {
        if (telNum.charAt(0) == '0') {
            return "82" + telNum.substring(1);
        } else {
            return telNum;
        }
    }

    public static String[] parseChatItemDate(String millisecond) {
        return parseChatItemDate(Long.parseLong(millisecond));
    }

    // 저장 된 millisecond 값을 가지고 ChatItemView 에 필요한 날짜 문자열을 배열로 저장함
    // 0 - yyyy년 mm 월 dd일 e 요일
    // 1 - 오전/오후 hh:mm
    public static String[] parseChatItemDate(long millisecond) {
        String[] result = new String[2];
        result[0] = DateFormat.getDateInstance(DateFormat.FULL).format(new Date(millisecond));
        result[1] = DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date(millisecond));
        return result;
    }

    public static boolean isEqualsChatItemDate(String before, String current) {
        return isEqualsChatItemDate(Long.parseLong(before), Long.parseLong(current));
    }

    // 이전 채팅 데이터의 날짜와 비교하여, 같은 날 인지 비교하는 메소드
    public static boolean isEqualsChatItemDate(long before, long current) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(before);

        int beforeYear = c.get(Calendar.YEAR);
        int beforeDayOfYear = c.get(Calendar.DAY_OF_YEAR);

        c.setTimeInMillis(current);

        int currentYear = c.get(Calendar.YEAR);
        int currentDayOfYear = c.get(Calendar.DAY_OF_YEAR);

        //년도를 비교하여 다를경우 false return
        if (beforeYear != currentYear) {
            return false;
        }
        //년도가 동일하고 DayOfYear 를 비교하여 다를경우 false return
        else if (beforeDayOfYear != currentDayOfYear) {
            return false;
        }
        //년도가 동일하고 DayOfYear 가 같으면 true return
        else {
            return true;
        }
    }
}