package honghong.imagemassenger.Model;

/**
 * Created by jun on 2016-01-12.
 */
public class JoinServerparsingModel {
    private String status;
    private String id;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString(){
        return "status = " +status +" // id = "+ id;
    }
}
