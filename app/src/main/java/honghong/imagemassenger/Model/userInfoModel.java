package honghong.imagemassenger.Model;

import java.io.Serializable;

/**
 * Created by jun on 2016-01-21.
 */
public class userInfoModel implements Serializable{
    private String id;
    private String telnum;
    private String userName;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setTelnum(String telnum) {
        this.telnum = telnum;
    }

    public String getTelnum() {
        return telnum;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public String toString(){
        return "userName = " + userName + " // id = " + id + " // telnum = "+ telnum;
    }
}
