package honghong.imagemassenger.Model;

import java.util.ArrayList;

/**
 * Created by jun on 2016-01-21.
 */
public class FriendListParsingModel {
    private String status;
    private ArrayList<userInfoModel> result;

    public void setResult(ArrayList<userInfoModel> result) {
        this.result = result;
    }

    public ArrayList<userInfoModel> getResult() {
        return result;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString(){
        return "status = "+ status + " // result[0] = {"+result.get(0).toString()+" } ";
    }
}
