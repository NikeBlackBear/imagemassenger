package honghong.imagemassenger.Model;

/**
 * Created by jun on 2016-01-28.
 */
public class ImageModel {
    private String id; //drawable내에서의 파일이름
    private String address; //주소
    private boolean checkState; // 갤러리에서 체크되어 있는 상태인지 아닌지 확인용.

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setCheckState(boolean checkState) {
        this.checkState = checkState;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public boolean getCheckState(){
        return checkState;
    }

    @Override
    public String toString(){
        return "id = " +id +" // address = "+ address +" // checkState = " + String.valueOf(checkState);
    }
}
