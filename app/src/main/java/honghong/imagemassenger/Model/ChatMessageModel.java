package honghong.imagemassenger.Model;

/**
 * Created by Administrator on 2016-01-22.
 */
public class ChatMessageModel {
    public String id;
    public String toId;
    public String fromId;
    public String imageId;
    public String dialogText;
    public String message;
    public String dialogTime;
    public String[] parsedTime;
    public int state;
}
