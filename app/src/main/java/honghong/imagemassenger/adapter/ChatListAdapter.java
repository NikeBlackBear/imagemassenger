package honghong.imagemassenger.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import honghong.imagemassenger.Model.ChatMessageModel;
import honghong.imagemassenger.Model.userInfoModel;
import honghong.imagemassenger.R;
import honghong.imagemassenger.widget.ChatItemView;

/**
 * Created by Administrator on 2015-12-24.
 */
public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder> {

    private Activity mActivity;
    private List<ChatMessageModel> mDataList;
    private userInfoModel mTargetFriendInfo;
    private RecyclerView mBindedRecyclerView;

    public ChatListAdapter(Activity activity, List<ChatMessageModel> dataList, userInfoModel friend, RecyclerView view) {
        mActivity = activity;
        mDataList = dataList;
        mTargetFriendInfo = friend;
        mBindedRecyclerView = view;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ChatItemView chatItemView;

        public ViewHolder(View v) {
            super(v);
            chatItemView = (ChatItemView) v.findViewById(R.id.chatItemParent);
            chatItemView.setController(ChatListAdapter.this, mBindedRecyclerView);
            chatItemView.attachFrendInfo(mTargetFriendInfo);
        }
    }

    public void onSendComplete(int position, String dialogId, boolean success) {
        ChatMessageModel model = mDataList.get(position);
        model.id = dialogId;
        model.state = success ? ChatItemView.STATE_SUCCESS : ChatItemView.STATE_FAILED;
        mDataList.set(position, model);
        notifyItemChanged(position);
    }

    @Override
    public ChatListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.chatItemView.bindView(position, mDataList.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public ChatMessageModel getItem(int position) {
        return mDataList.get(position);
    }
}