package honghong.imagemassenger.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;

import honghong.imagemassenger.R;
import honghong.imagemassenger.widget.CircleNetworkImageView;

/**
 * Created by Administrator on 2015-12-24.
 */
public class ChatRoomListAdapter extends RecyclerView.Adapter<ChatRoomListAdapter.ViewHolder> {

    private String testUrl[] = {"http://cfile8.uf.tistory.com/image/113729374FBD753A05A911",
            "http://kimchibilliards.com/technote7/data/board/m0203/file_in_body/1/zn5.jpg",
            "http://mblogthumb3.phinf.naver.net/20141202_134/ace7770320_1417494153009HaA8u_PNG/%B5%F1_%BE%DF%BD%BA%C6%DB%BD%BA.png?type=w2",
            "http://www.carompark.com/attach/1/7733247106.jpg"};

    private Activity mActivity;
    private OnChatRoomListItemClickListener mListener;

    public interface OnChatRoomListItemClickListener {
        public void onItemClick(ViewHolder vh, int position);
    }

    public ChatRoomListAdapter(Activity activity) {
        mActivity = activity;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RelativeLayout chatRoomFrame;
        public CircleNetworkImageView chatRoomMemberProfileThumb;
        public TextView chatRoomMember;
        public TextView chatRoomRepresentMessage;
        public TextView chatRoomDate;

        public ViewHolder(View v) {
            super(v);
            chatRoomFrame = (RelativeLayout) v.findViewById(R.id.chat_room_frame);
            chatRoomMemberProfileThumb = (CircleNetworkImageView) v.findViewById(R.id.chat_room_profile_image);
            chatRoomMember = (TextView) v.findViewById(R.id.chat_room_member_name);
            chatRoomRepresentMessage = (TextView) v.findViewById(R.id.chat_room_represent_message);
            chatRoomDate = (TextView) v.findViewById(R.id.chat_room_date);
        }
    }

    @Override
    public ChatRoomListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_room_list_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.chatRoomFrame.setTag(R.string.key_item_position, position);
        holder.chatRoomFrame.setOnClickListener(mChatItemFrameClickListener);
        holder.chatRoomMemberProfileThumb.loadCircleBitmap(testUrl[new Random().nextInt(4)]);

        if (position % 2 == 0) {
            holder.chatRoomMember.setText("진수민,이승준,전현욱,이진협,하성재");
            holder.chatRoomDate.setText("2015/12/25");
            holder.chatRoomRepresentMessage.setText("매니아 당구장 7시");
        } else {
            holder.chatRoomMember.setText("진수민,이승준");
            holder.chatRoomDate.setText("오후 6:04");
            holder.chatRoomRepresentMessage.setText("지혜가 깊은 사람은 자기에게 무슨 이익이 있을까 해서\n" +
                    "또는 이익이 있으므로 해서 사랑하는 것이 아니다.\n" +
                    "사랑한다는 그 자체 속에 행복을 느낌으로 해서 사랑하는 것이다.");
        }
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public void setOnChatItemFrameClickListener(OnChatRoomListItemClickListener listener) {
        mListener = listener;
    }

    private final View.OnClickListener mChatItemFrameClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick((ViewHolder) ((View) v.getParent()).getTag(), (int) v.getTag(R.string.key_item_position));
            }
        }
    };
}