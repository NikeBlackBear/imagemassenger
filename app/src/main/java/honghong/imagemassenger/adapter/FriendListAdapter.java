package honghong.imagemassenger.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import honghong.imagemassenger.Model.userInfoModel;
import honghong.imagemassenger.R;
import honghong.imagemassenger.widget.CircleNetworkImageView;

/**
 * Created by jun on 2016-01-13.
 */
public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<userInfoModel> mList;
    private OnFriendListItemClickListener mListener;

    //test 더미 데이터
    private String testUrl[] = {"https://i.ytimg.com/vi/Xn1n0ioQzgk/maxresdefault.jpg", "http://image.cine21.com/resize/cine21/poster/2013/0312/16_11_35__513ed527b6adb[X230,261].jpg"
            , "http://cfile21.uf.tistory.com/image/2614263655784128267C36"};
    private String testName[] = {"진수민", "이승준", "개발자 형님"};
    private String testStatus[] = {"와썹", "조반조반", "꾀꼬리가 버들가지에 깃드니 조각 조각이 황금이오. 하늘과 땅에 광명이 가득하게 비치는"};

    public FriendListAdapter(Activity activity, ArrayList<userInfoModel> list) {
        mActivity = activity;
        mList = list;
    }

    public userInfoModel getItem(int position){
        return mList.get(position);
    }

    public interface OnFriendListItemClickListener {
        public void onItemClick(userInfoModel data);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout FriendFrame;
        public CircleNetworkImageView profilePhoto;
        public TextView username;
        public TextView status;

        public ViewHolder(View v) {
            super(v);
            FriendFrame = (RelativeLayout) v.findViewById(R.id.friend_list_frame);
            profilePhoto = (CircleNetworkImageView) v.findViewById(R.id.profile_photo);
            username = (TextView) v.findViewById(R.id.user_name);
            status = (TextView) v.findViewById(R.id.status);
        }
    }

    @Override
    public FriendListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_list_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.FriendFrame.setTag(R.string.key_item_position, position);
        holder.FriendFrame.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onItemClick(mList.get(position));
                }
            }
        });
        holder.profilePhoto.loadCircleBitmap(testUrl[position%3]);
        holder.username.setText(mList.get(position).getUserName());
        holder.status.setText(mList.get(position).getTelnum());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setOnFriendListItemClickListener(OnFriendListItemClickListener listener) {
        mListener = listener;
    }

}
