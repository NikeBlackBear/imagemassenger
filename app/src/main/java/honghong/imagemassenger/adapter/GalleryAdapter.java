package honghong.imagemassenger.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import honghong.imagemassenger.Model.ImageModel;
import honghong.imagemassenger.R;
import honghong.imagemassenger.common.CommonValues;
import honghong.imagemassenger.util.LogUtil;

/**
 * Created by jun on 2016-01-28.
 */
public class GalleryAdapter extends BaseAdapter implements CommonValues{
    private LayoutInflater inflater;
    private ArrayList<ImageModel> mList;
    private ImageLoader mImageLoader;
    private Context mContext;
    private onImageClickListener mOnImageClickListener;
    private boolean CheckedImgFlag= false;

    public GalleryAdapter(Context c, ArrayList<ImageModel> imageList) {
        inflater = LayoutInflater.from(c);
        mList = imageList;
        mContext = c;
        mImageLoader = ImageLoader.getInstance();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public ImageModel getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.gallery_block, parent, false);
            holder = new ViewHolder();
            holder.Image = (ImageView) view.findViewById(R.id.image);
            holder.CheckContainer = (ImageView) view.findViewById(R.id.check);
            holder.progressBar = (ProgressBar) view.findViewById(R.id.progress);
            holder.Image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (Integer) v.getTag();
                    if (mList.get(position).getCheckState()) {
                            holder.CheckContainer.setVisibility(View.GONE);
                            CheckedImgFlag = false;
                    } else {
                        if (!CheckedImgFlag){
                            holder.CheckContainer.setVisibility(View.VISIBLE);
                            CheckedImgFlag = true;
                        }
                    }
                    mOnImageClickListener.onClick(mList.get(position), position);
                }
            });
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.Image.setTag(position);
        if (mList.get(position).getCheckState()) {
            holder.CheckContainer.setVisibility(View.VISIBLE);
        } else {
            holder.CheckContainer.setVisibility(View.GONE);
        }
        LogUtil.LOGE("GalleryAdapter :: imgAdd = " + mList.get(position).getAddress());
        mImageLoader.displayImage(mList.get(position).getAddress(), holder.Image, IMAGE_LODER_OPTION, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                holder.progressBar.setProgress(0);
                holder.progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                holder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.progressBar.setVisibility(View.GONE);
            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
                holder.progressBar.setProgress(Math.round(100.0f*current/ total));
            }
        });
        return view;
    }

    public interface onImageClickListener {
        public void onClick(ImageModel data, int position);
    }

    public void setOnImageClickListener(onImageClickListener listener) {
        mOnImageClickListener = listener;
    }

    private class ViewHolder {
        ImageView Image;
        ImageView CheckContainer;
        ProgressBar progressBar;
    }
}
