package honghong.imagemassenger.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Administrator on 2015-02-06.
 */
public class MessageDB extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    public static final String TABLE = "message_table";

    public class Columns {
        //내부DB Primary Key
        public static final String ID = "_id";
        //Message ID
        public static final String MESSAGE_ID = "message_id";
        //대화 상대 사용자 ID
        public static final String PARTY_ID = "party_id";
        //받는 사람 ID
        public static final String TO_ID = "to_id";
        //보낸 사람 ID
        public static final String FROM_ID = "from_id";
        //Image ID
        public static final String IMAGE_ID = "imageid";
        //대화내용
        public static final String DIALOG_TEXT = "dialogText";
        //날짜
        public static final String DATE = "date";
    }

    private String createSql = "create table " + TABLE + " ("
            + Columns.ID + " integer primary key autoincrement, "
            + Columns.MESSAGE_ID + " integer, "
            + Columns.PARTY_ID + " integer, "
            + Columns.TO_ID + " integer, "
            + Columns.FROM_ID + " integer, "
            + Columns.IMAGE_ID + " text, "
            + Columns.DIALOG_TEXT + " text, "
            + Columns.DATE + " integer)";

    public MessageDB(Context context) {
        super(context, "MessageDB", null, VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase arg0) {
        arg0.execSQL(createSql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}