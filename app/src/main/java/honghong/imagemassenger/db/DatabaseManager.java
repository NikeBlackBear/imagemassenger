package honghong.imagemassenger.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import honghong.imagemassenger.Model.ChatMessageModel;
import honghong.imagemassenger.util.LogUtil;
import honghong.imagemassenger.util.Utils;
import honghong.imagemassenger.widget.ChatItemView;

/**
 * Created by Administrator on 2015-02-05.
 */
public class DatabaseManager {
    private MessageDB mMessageDBHelper;
    private SQLiteDatabase mMessageDB;
    private static DatabaseManager mInstance;

    public static DatabaseManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DatabaseManager(context);
        }
        return mInstance;
    }

    private DatabaseManager(Context context) {
        mMessageDBHelper = new MessageDB(context);
        mMessageDB = mMessageDBHelper.getWritableDatabase();
    }

    public void close() {
        mMessageDBHelper.close();
    }

    public void insertChatRowData(String partyId, ChatMessageModel chat) {
        ContentValues values = new ContentValues();
        values.put(MessageDB.Columns.PARTY_ID, partyId);
        values.put(MessageDB.Columns.MESSAGE_ID, chat.id);
        values.put(MessageDB.Columns.TO_ID, chat.toId);
        values.put(MessageDB.Columns.FROM_ID, chat.fromId);
        values.put(MessageDB.Columns.IMAGE_ID, chat.imageId);
        values.put(MessageDB.Columns.DIALOG_TEXT, chat.dialogText);
        values.put(MessageDB.Columns.DATE, Long.parseLong(chat.dialogTime));
        LogUtil.LOGE("insertChatRowData :: dialogTIme = " + chat.dialogTime);
        mMessageDB.insert(MessageDB.TABLE, null, values);
    }

    public List<ChatMessageModel> getChatRowData(String partyId) {
        List<ChatMessageModel> rows = new ArrayList<ChatMessageModel>();
        Cursor cursor = mMessageDB.query(MessageDB.TABLE, null, MessageDB.Columns.PARTY_ID + " = '" + partyId + "'", null, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    ChatMessageModel row = new ChatMessageModel();
                    row.id = String.valueOf(cursor.getInt(cursor.getColumnIndex(MessageDB.Columns.MESSAGE_ID)));
                    row.toId = String.valueOf(cursor.getInt(cursor.getColumnIndex(MessageDB.Columns.TO_ID)));
                    row.fromId = String.valueOf(cursor.getInt(cursor.getColumnIndex(MessageDB.Columns.FROM_ID)));
                    row.imageId = cursor.getString(cursor.getColumnIndex(MessageDB.Columns.IMAGE_ID));
                    row.dialogText = cursor.getString(cursor.getColumnIndex(MessageDB.Columns.DIALOG_TEXT));
                    row.dialogTime = String.valueOf(cursor.getLong(cursor.getColumnIndex(MessageDB.Columns.DATE)));
                    row.parsedTime = Utils.parseChatItemDate(row.dialogTime);
                    row.state = ChatItemView.STATE_SUCCESS;
                    rows.add(row);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return rows;
    }

    //마지막 수신 메세지 id 를 불러오는 메소드
    public String getLastReceiveMessageId(String userId) {
        Cursor cursor = mMessageDB.query(MessageDB.TABLE, null, MessageDB.Columns.TO_ID + " = '" + userId + "'", null, null, null, MessageDB.Columns.MESSAGE_ID + " ASC");
        if (cursor != null) {
            if (cursor.moveToLast()) {
                return cursor.getString(cursor.getColumnIndex(MessageDB.Columns.MESSAGE_ID));
            }
            cursor.close();
        }
        return "0";
    }

    public void printChatDB(String partyId) {
        List<ChatMessageModel> rows = getChatRowData(partyId);
        LogUtil.LOGE("printChatDB DBTEST Size == " + rows.size());
        for (int i = 0; i < rows.size(); i++) {
            ChatMessageModel row = rows.get(i);
            LogUtil.LOGE("----row[" + i + "]----");
            LogUtil.LOGE("id = " + row.id);
            LogUtil.LOGE("toId = " + row.toId);
            LogUtil.LOGE("fromId = " + row.fromId);
            LogUtil.LOGE("imageId = " + row.imageId);
            LogUtil.LOGE("dialogText = " + row.dialogText);
            LogUtil.LOGE("date = " + row.dialogTime);
            LogUtil.LOGE("----END----");
        }
    }
}