package honghong.imagemassenger.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import honghong.imagemassenger.Activity.TalkRoomActivity;
import honghong.imagemassenger.Model.ChatMessageModel;
import honghong.imagemassenger.Model.FriendListParsingModel;
import honghong.imagemassenger.Model.RequestChatMessageRSP;
import honghong.imagemassenger.Model.userInfoModel;
import honghong.imagemassenger.R;
import honghong.imagemassenger.adapter.FriendListAdapter;
import honghong.imagemassenger.common.CommonValues;
import honghong.imagemassenger.db.DatabaseManager;
import honghong.imagemassenger.proxy.ParseManager;
import honghong.imagemassenger.proxy.ServerProxy;
import honghong.imagemassenger.util.LogUtil;
import honghong.imagemassenger.util.MessengerPreferences;

public class FriendListFragment extends Fragment implements CommonValues {
    private RecyclerView mFriendListRecycleerView, talk_room_list;
    private LinearLayoutManager mLayoutManager1, mLayoutManager2;
    private FriendListAdapter mFriendListAdapter;
    private TextView groupTalkRoomListBtn;
    private boolean groupTalkRoomListOpenCloseFlag;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.friend_list_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    public void init() {
        groupTalkRoomListOpenCloseFlag = false;

        View view = getView();
        ServerProxy.getInstance(getActivity()).addFriendList(addFriendListCallback);

        //앱 실행 시 새로운 수신메세지가 있는지 체크
        ServerProxy.getInstance(getActivity()).requestGetAllReceiveMessage(mGetChatMessageResponseCallback, MessengerPreferences.getUserId(getActivity()),
                DatabaseManager.getInstance(getActivity()).getLastReceiveMessageId(MessengerPreferences.getUserId(getActivity())));

        mLayoutManager1 = new LinearLayoutManager(getActivity());
        mLayoutManager2 = new LinearLayoutManager(getActivity());

        groupTalkRoomListBtn = (TextView) view.findViewById(R.id.talk_room_list_open_close_btn);
        groupTalkRoomListBtn.setOnClickListener(groupTalkRoomListBtnClickListener);

        talk_room_list = (RecyclerView) view.findViewById(R.id.talk_room_list);
        talk_room_list.setHasFixedSize(false);
        talk_room_list.setLayoutManager(mLayoutManager1);

        mFriendListRecycleerView = (RecyclerView) view.findViewById(R.id.friend_list);
        mFriendListRecycleerView.setHasFixedSize(false);
        mFriendListRecycleerView.setLayoutManager(mLayoutManager2);
    }

    private View.OnClickListener groupTalkRoomListBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (groupTalkRoomListOpenCloseFlag) {
                Toast.makeText(getActivity(), "닫기", Toast.LENGTH_SHORT).show();
                talk_room_list.setVisibility(View.GONE);
                groupTalkRoomListOpenCloseFlag = false;
            } else {
                talk_room_list.setVisibility(View.VISIBLE);
                Toast.makeText(getActivity(), "열기", Toast.LENGTH_SHORT).show();
                groupTalkRoomListOpenCloseFlag = true;
            }
        }
    };

    private final FriendListAdapter.OnFriendListItemClickListener mFriendListItemListener = new FriendListAdapter.OnFriendListItemClickListener() {
        @Override
        public void onItemClick(userInfoModel data) {
            Intent intent = new Intent(getActivity(), TalkRoomActivity.class);
//            intent.putExtra("UserName", data.getUserName());
//            intent.putExtra("UserId", data.getId());
//            intent.putExtra("UserPhone", data.getTelnum());
            intent.putExtra("UserInfo", data);
            getActivity().startActivity(intent);
        }
    };

    private ServerProxy.ResponseCallback addFriendListCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (getActivity() != null) {
                LogUtil.LOGV("FriendListFragment :: addFriendListCallback :: onResponse() :: response = " + response);
                FriendListParsingModel Modeltemp = new ParseManager().parsingAddFriend(response);
                if (Modeltemp.getStatus().equals("0")) {
                    mFriendListAdapter = new FriendListAdapter(getActivity(), Modeltemp.getResult());
                    mFriendListAdapter.setOnFriendListItemClickListener(mFriendListItemListener);
                    mFriendListRecycleerView.setAdapter(mFriendListAdapter);
                    LogUtil.LOGE("친구 불러오기 성공");
                } else {
                    LogUtil.LOGV("FriendListFragment :: addFriendListCallback :: ServerParsing fail");
                }
            } else {
                LogUtil.LOGV("FriendListFragment :: addFriendListCallback :: not exists next data");
            }
        }

        @Override
        public void onError(int status) {
            LogUtil.LOGV("Join :: onError() :: status =" + status);
        }
    };

    private final ServerProxy.ResponseCallback mGetChatMessageResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            LogUtil.LOGE("mGetChatMessageResponseCallback :: onResponse() :: response = " + response);
            RequestChatMessageRSP rsp = new ParseManager().parseAllReceiveMessageResponse(response);
            if (rsp.status.equals(RESPONSE_OK)) {
                //새로운 수신 메세지가 있을 경우
                if (rsp.result.size() > 0) {
                    for (int i = 0; i < rsp.result.size(); i++) {
                        //모든 새로운 수신 메세지를 데이터베이스에 저장한다. 이때, 1:1 대화이고 수신 메세지 이므로 partyid 는 항상 보낸사람 id 가 된다
                        ChatMessageModel item = rsp.result.get(i);
                        DatabaseManager.getInstance(getActivity()).insertChatRowData(item.fromId, item);
                    }
                }
            }
        }

        @Override
        public void onError(int status) {
            LogUtil.LOGE("mGetChatMessageResponseCallback :: onError() :: status = " + status);
        }
    };
}