package honghong.imagemassenger.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import honghong.imagemassenger.Activity.Join;
import honghong.imagemassenger.R;
import honghong.imagemassenger.proxy.ServerProxy;

/**
 * Created by jun on 2016-01-21.
 */
public class SettingFragment extends Fragment {

    private TextView joinTest; // test용 버튼.


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.setting_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init(){
        joinTest = (TextView) getActivity().findViewById(R.id.joinBtn);
        joinTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Join.class);
                startActivity(intent);
            }
        });


    }
}
