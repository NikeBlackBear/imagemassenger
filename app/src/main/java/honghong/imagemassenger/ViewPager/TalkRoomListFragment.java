package honghong.imagemassenger.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import honghong.imagemassenger.Activity.TalkRoomActivity;
import honghong.imagemassenger.R;
import honghong.imagemassenger.adapter.ChatRoomListAdapter;

/**
 * Created by jun on 2015-12-17.
 */
public class TalkRoomListFragment extends Fragment {

    private RecyclerView mChatRoomListRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ChatRoomListAdapter mChatRoomListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.talkroom_list_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        mLayoutManager = new LinearLayoutManager(getActivity());

        mChatRoomListRecyclerView = (RecyclerView) getView().findViewById(R.id.chat_room_list);
        mChatRoomListRecyclerView.setHasFixedSize(false);
        mChatRoomListRecyclerView.setLayoutManager(mLayoutManager);

        mChatRoomListAdapter = new ChatRoomListAdapter(getActivity());
        mChatRoomListAdapter.setOnChatItemFrameClickListener(mChatRoomListItemListener);
        mChatRoomListRecyclerView.setAdapter(mChatRoomListAdapter);
    }

    private final ChatRoomListAdapter.OnChatRoomListItemClickListener mChatRoomListItemListener = new ChatRoomListAdapter.OnChatRoomListItemClickListener() {
        @Override
        public void onItemClick(ChatRoomListAdapter.ViewHolder vh, int position) {
            Intent intent = new Intent(getActivity(), TalkRoomActivity.class);
            getActivity().startActivity(intent);
        }
    };
}
