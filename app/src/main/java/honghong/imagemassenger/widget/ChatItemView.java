package honghong.imagemassenger.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;
import honghong.imagemassenger.Model.ChatMessageModel;
import honghong.imagemassenger.Model.SendChatMessageRSP;
import honghong.imagemassenger.Model.userInfoModel;
import honghong.imagemassenger.R;
import honghong.imagemassenger.adapter.ChatListAdapter;
import honghong.imagemassenger.common.CommonValues;
import honghong.imagemassenger.db.DatabaseManager;
import honghong.imagemassenger.proxy.ParseManager;
import honghong.imagemassenger.proxy.ServerProxy;
import honghong.imagemassenger.util.LogUtil;
import honghong.imagemassenger.util.Utils;
import honghong.imagemassenger.widget.shader.BubbleImageView;
import honghong.imagemassenger.widget.shader.BubbleShader;

/**
 * Created by Administrator on 2015-12-25.
 */
public class ChatItemView extends RelativeLayout implements CommonValues {

    public static final int STATE_FAILED = 0;
    public static final int STATE_LOADING = 1;
    public static final int STATE_SUCCESS = 2;

    private RelativeLayout mDateTitleSection;
    private TextView mDateTitleText;
    private RelativeLayout mChatTitleSection;
    private CircleNetworkImageView mProfileImg;
    private TextView mUserName;
    private TextView mChatDate;
    private BubbleImageView mChatBackground;
    private TextView mChatMessage;
    private RelativeLayout mChatRowStateFrame;
    private CircularProgressBar mSendLoadingBar;
    private RelativeLayout mFailedActionFrame;

    private ChatMessageModel mTargetChatData;
    private int mPosition;
    private ChatListAdapter mControlAdapter;
    private RecyclerView mRecyclerView;
    private userInfoModel mTargetFriendInfo;

    public ChatItemView(Context context) {
        super(context);
    }

    public ChatItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ChatItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mDateTitleSection = (RelativeLayout) findViewById(R.id.date_title_section);
        mDateTitleText = (TextView) findViewById(R.id.date_title);
        mChatTitleSection = (RelativeLayout) findViewById(R.id.chat_title_section);
        mProfileImg = (CircleNetworkImageView) findViewById(R.id.chat_profile_image);
        mUserName = (TextView) findViewById(R.id.chat_user_name);
        mChatDate = (TextView) findViewById(R.id.chat_date);
        mChatBackground = (BubbleImageView) findViewById(R.id.chat_bg);
        mChatMessage = (TextView) findViewById(R.id.chat_message);
        mChatRowStateFrame = (RelativeLayout) findViewById(R.id.chat_row_frame);
        mSendLoadingBar = (CircularProgressBar) findViewById(R.id.send_loading_bar);
        mFailedActionFrame = (RelativeLayout) findViewById(R.id.failed_section);
    }

    //ItemView 를 제어하기 위한 adapter 와 RecyclerView 객체를 설정
    public void setController(ChatListAdapter adapter, RecyclerView view) {
        mControlAdapter = adapter;
        mRecyclerView = view;
    }

    public void send() {
        ServerProxy.getInstance(getContext()).requestSendChatMessage(mSendMessageResponseCallback,
                mTargetChatData.fromId,
                mTargetChatData.toId,
                mTargetChatData.imageId,
                mTargetChatData.dialogText);
    }

    public void attachFrendInfo(userInfoModel friend) {
        mTargetFriendInfo = friend;
    }

    public ChatMessageModel getTargetChatData() {
        return mTargetChatData;
    }

    public void bindView(int position, ChatMessageModel chatData) {
        mPosition = position;
        mTargetChatData = chatData;

        //두번째 이상의 item 일 경우 이전 position 의 date 와 비교하여 Date Title 의 Visible 여부를 체크함
        if (position > 0) {
            if (Utils.isEqualsChatItemDate(Long.parseLong(mControlAdapter.getItem(position - 1).dialogTime), Long.parseLong(mTargetChatData.dialogTime))) {
                mDateTitleSection.setVisibility(View.GONE);
            } else {
                mDateTitleSection.setVisibility(View.VISIBLE);
            }
        }
        //첫번째 item 일 경우 무조건 Date Title 을 Visible 함
        else {
            mDateTitleSection.setVisibility(View.VISIBLE);
        }

        mDateTitleText.setText(chatData.parsedTime[0]);

        mChatBackground.setAlpha(chatData.state == STATE_SUCCESS ? 1.0f : 0.5f);
        mChatMessage.setAlpha(chatData.state == STATE_SUCCESS ? 1.0f : 0.5f);
        mChatRowStateFrame.setVisibility(chatData.state == STATE_SUCCESS ? View.GONE : View.VISIBLE);
        mFailedActionFrame.setVisibility(chatData.state == STATE_FAILED ? View.VISIBLE : View.GONE);
        mSendLoadingBar.setVisibility(chatData.state == STATE_LOADING ? View.VISIBLE : View.GONE);

        //TODO :: 프로필 이미지
        mProfileImg.loadCircleBitmap("http://kimchibilliards.com/technote7/data/board/m0203/file_in_body/1/zn5.jpg");
        mUserName.setText(mTargetFriendInfo.getUserName());

        //TODO :: 채팅 날짜 설정
        mChatDate.setText(chatData.parsedTime[1]);

        mChatMessage.setText(chatData.dialogText);
        //mChatBackground.setImageResource(Utils.getResourceId(getContext(), chatData.imageId));
        ImageLoader.getInstance().displayImage(("drawable://" + Utils.getResourceId(getContext(), chatData.imageId)), mChatBackground);

        //TODO:: id 매칭을 통해 send/receive 판별 해야함. 현재 push 받는 부분이 안되있어서 임의로 테스트 함
        if (position % 2 == 0) {
            mChatTitleSection.setVisibility(View.VISIBLE);
            mChatBackground.setArrowPosition(BubbleShader.ArrowPosition.LEFT);

            mChatDate.setGravity(Gravity.RIGHT);
        } else {
            mChatTitleSection.setVisibility(View.GONE);
            mChatBackground.setArrowPosition(BubbleShader.ArrowPosition.RIGHT);

            mChatDate.setGravity(Gravity.LEFT);
        }
    }

    private final ServerProxy.ResponseCallback mSendMessageResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            LogUtil.LOGE("mSendMessageResponseCallback :: onResponse() :: response = " + response);
            SendChatMessageRSP rsp = new ParseManager().parseSendChatMessageResponse(response);
            if (rsp.status.equals(RESPONSE_OK)) {
                //Adapter 에 완료 이벤트를 알리고 리스트를 갱신한다
                mControlAdapter.onSendComplete(mPosition, rsp.dialogid, true);

                //response 받은 메세지 id 를 채팅 메세지 객체에 갱신한다
                mTargetChatData.id = rsp.dialogid;

                //TODO :: 서버에서 받은 dialogTime 으로 설정 해야함
                mTargetChatData.dialogTime = String.valueOf(System.currentTimeMillis());

                //데이터베이스에 메세지를 저장 한다. 이때, partyid 는 1:1 채팅이므로 항상 받는 사람으로 설정 하였다.
                DatabaseManager.getInstance(getContext()).insertChatRowData(mTargetChatData.toId, mTargetChatData);

                //TODO :: DB Print TEST
                DatabaseManager.getInstance(getContext()).printChatDB(mTargetChatData.toId);
            } else {
                mControlAdapter.onSendComplete(mPosition, "-1", false);
            }
        }

        @Override
        public void onError(int status) {
            LogUtil.LOGE("mSendMessageResponseCallback :: onError() :: status = " + status);
            mControlAdapter.onSendComplete(mPosition, "-1", false);
        }
    };
}