package honghong.imagemassenger.widget;

import java.util.Observable;

/**
 * Created by Administrator on 2016-01-13.
 */
public class ChatContentObservable extends Observable {

    public void changeChatMessage(String message) {
        setChanged();
        notifyObservers(message);
    }
}
