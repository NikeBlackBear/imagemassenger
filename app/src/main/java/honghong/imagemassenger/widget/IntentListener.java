package honghong.imagemassenger.widget;

import android.content.Intent;

/**
 * Created by Administrator on 2016-02-03.
 */
public interface IntentListener {
    public void deliveryActivityResult(int requestCode, int resultCode, Intent data);
}
