package honghong.imagemassenger.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import honghong.imagemassenger.Activity.Gallerly;
import honghong.imagemassenger.Model.ChatMessageModel;
import honghong.imagemassenger.R;
import honghong.imagemassenger.util.LogUtil;
import honghong.imagemassenger.util.Utils;

/**
 * Created by Administrator on 2016-01-11.
 */
public class ComposeChatContentView extends RelativeLayout implements Observer, IntentListener {

    public static final int REQUEST_CODE_GALLERY = 100;

    private List<Integer> mBackgroundResIdList;
    private ImageView mChatBackgroundView;
    private TextView mChatContentView;
    private ChatContentObservable mObservable;
    private ImageButton mGalleryBtn;
    private ImageView mBgSelectLeftBtn;
    private ImageView mBgSelectRightBtn;
    private int mCurrentSelectPosition = -1;
    private int mGalleryState = GalleryProcessSate.STATE_IDLE;

    public class GalleryProcessSate {
        public static final int STATE_IDLE = 0;
        public static final int STATE_PICK = 1;
    }

    public ComposeChatContentView(Context context) {
        this(context, null);
    }

    public ComposeChatContentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LogUtil.LOGE("ComposeChatContentView :: onFinishInflate()");
        mBackgroundResIdList = Utils.getChatBackgroundResIds(getContext());
        mCurrentSelectPosition = new Random().nextInt(mBackgroundResIdList.size());

        mChatBackgroundView = (ImageView) findViewById(R.id.chat_bg);
        mBgSelectLeftBtn = (ImageView) findViewById(R.id.bg_select_left_btn);
        mBgSelectRightBtn = (ImageView) findViewById(R.id.bg_select_right_btn);
        mChatContentView = (TextView) findViewById(R.id.chat_content);
        mGalleryBtn = (ImageButton) findViewById(R.id.test_gallery_btn);
        mGalleryBtn.setOnClickListener(mGalleryBtnClickListener);
        mBgSelectRightBtn.setOnClickListener(mBgSelectBtnClickListener);
        mBgSelectLeftBtn.setOnClickListener(mBgSelectBtnClickListener);
        changeBackground(mCurrentSelectPosition);

        if (mCurrentSelectPosition == 0) {
            mBgSelectLeftBtn.setVisibility(View.GONE);
        } else if (mCurrentSelectPosition == mBackgroundResIdList.size() - 1) {
            mBgSelectRightBtn.setVisibility(View.GONE);
        }
    }

    public void registerObserver(ChatContentObservable observable) {
        mObservable = observable;
        mObservable.addObserver(this);
    }

    public void unregisterObserver() {
        if (mObservable != null) {
            mObservable.deleteObserver(this);
        }
    }

    private void changeBackground(int position) {
        //mChatBackgroundView.setImageResource(mBackgroundResIdList.get(position));
        ImageLoader.getInstance().displayImage("drawable://" + mBackgroundResIdList.get(position), mChatBackgroundView);
    }

    public ChatMessageModel createComposeData() {
        ChatMessageModel result = new ChatMessageModel();
        //TODO :: toId 와 fromid 실 데이터 필요 (TalkRoomActivity 에서 세팅 하기로함)
        result.imageId = Utils.getResourceName(getContext(), mBackgroundResIdList.get(mCurrentSelectPosition));
        result.dialogText = mChatContentView.getText().toString();

        //TODO :: date 검토
        result.dialogTime = String.valueOf(System.currentTimeMillis());
        return result;
    }

    private String getSelectedBackgroundResourceName() {
        return Utils.getResourceName(getContext(), mBackgroundResIdList.get(mCurrentSelectPosition));
    }

    public int getGalleryProcessState() {
        return mGalleryState;
    }

    public void setGalleryProcessState(int state) {
        mGalleryState = state;
    }

    @Override
    public void update(Observable observable, Object data) {
        LogUtil.LOGE("ComposeChatContentView :: update() :: data = " + data + " equals = " + observable.equals(mObservable));
        mChatContentView.setText((String) data);
    }

    @Override
    public void deliveryActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtil.LOGE("deliveryActivityResult");
        if (requestCode == ComposeChatContentView.REQUEST_CODE_GALLERY) {
            if (resultCode == Activity.RESULT_OK) {
                LogUtil.LOGE("resultImgID = " + data.getExtras().getString("resultImgID"));
                LogUtil.LOGE("resultImgPosition = " + data.getExtras().getInt("resultImgPosition"));
                mCurrentSelectPosition = data.getExtras().getInt("resultImgPosition");
                changeBackground(mCurrentSelectPosition);

                mBgSelectLeftBtn.setVisibility(View.VISIBLE);
                mBgSelectRightBtn.setVisibility(View.VISIBLE);
                if (mCurrentSelectPosition == 0) {
                    mBgSelectLeftBtn.setVisibility(View.GONE);
                } else if (mCurrentSelectPosition == mBackgroundResIdList.size() - 1) {
                    mBgSelectRightBtn.setVisibility(View.GONE);
                }
            }
        }
    }

    private final View.OnClickListener mGalleryBtnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mGalleryState = GalleryProcessSate.STATE_PICK;
            Intent intent = new Intent(getContext(), Gallerly.class);
            ((Activity) getContext()).startActivityForResult(intent, REQUEST_CODE_GALLERY);
        }
    };

    private final View.OnClickListener mBgSelectBtnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.equals(mBgSelectLeftBtn)) {
                if (mCurrentSelectPosition > 0) {
                    mCurrentSelectPosition--;
                    if (mCurrentSelectPosition == 0) {
                        mBgSelectLeftBtn.setVisibility(View.GONE);
                    } else if (mCurrentSelectPosition == mBackgroundResIdList.size() - 2) {
                        mBgSelectRightBtn.setVisibility(View.VISIBLE);
                    }
                }
            } else if (v.equals(mBgSelectRightBtn)) {
                if (mCurrentSelectPosition < mBackgroundResIdList.size() - 1) {
                    mCurrentSelectPosition++;
                    if (mCurrentSelectPosition == 1) {
                        mBgSelectLeftBtn.setVisibility(View.VISIBLE);
                    } else if (mCurrentSelectPosition == mBackgroundResIdList.size() - 1) {
                        mBgSelectRightBtn.setVisibility(View.GONE);
                    }
                }
            }
            changeBackground(mCurrentSelectPosition);
        }
    };
}