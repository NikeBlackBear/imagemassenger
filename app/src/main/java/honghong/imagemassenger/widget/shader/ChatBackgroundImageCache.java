package honghong.imagemassenger.widget.shader;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import honghong.imagemassenger.util.LogUtil;

/**
 * Created by Administrator on 2016-01-20.
 */
public class ChatBackgroundImageCache {

    private LruCache<String, Bitmap> lruCache;
    public ChatBackgroundImageCache(int maxCount) {
        lruCache = new LruCache<String, Bitmap>(maxCount);
    }

    public void putBitmap(String key, Bitmap bitmap) {
        LogUtil.LOGE("ChatBackgroundImageCache :: addBitmap() :: key = " + key + " bitmap = " + bitmap);
        if (bitmap == null)
            return;
        lruCache.put(key, bitmap);
    }

    public Bitmap getBitmap(String key) {
        LogUtil.LOGE("ChatBackgroundImageCache :: getBitmap() :: key = " + key + " bitmap = " + lruCache.get(key));
        return lruCache.get(key);
    }

    public boolean isAlreadyCache(String key) {
        return lruCache.get(key) != null;
    }

    public void clear() {
        lruCache.evictAll();
    }

}
