package honghong.imagemassenger.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by Administrator on 2016-01-11.
 */
public class SoftKeyboardDetectorView extends RelativeLayout {
    private boolean mShownKeyboard;
    private OnKeyboardStateListener mListener;

    public interface OnKeyboardStateListener {
        public void onShowSoftKeyboard();

        public void onHiddenSoftKeyboard();
    }

    public SoftKeyboardDetectorView(Context context) {
        this(context, null);
    }

    public SoftKeyboardDetectorView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        Activity activity = (Activity) getContext();
        Rect rect = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        int statusBarHeight = rect.top;
        int screenHeight = activity.getWindowManager().getDefaultDisplay().getHeight();
        int diffHeight = (screenHeight - statusBarHeight) - h;
        if (diffHeight > 100 && !mShownKeyboard) {
            mShownKeyboard = true;
        } else if (diffHeight < 100 && mShownKeyboard) {
            mShownKeyboard = false;
        }

        if (mListener != null) {
            if (mShownKeyboard) {
                mListener.onShowSoftKeyboard();
            } else {
                mListener.onHiddenSoftKeyboard();
            }
        }
        super.onSizeChanged(w, h, oldw, oldh);
    }

    public void setOnKeyboardStateListener(OnKeyboardStateListener listener) {
        mListener = listener;
    }
}
