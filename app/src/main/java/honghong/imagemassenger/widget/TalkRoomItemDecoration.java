package honghong.imagemassenger.widget;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Administrator on 2016-01-16.
 */
public class TalkRoomItemDecoration extends RecyclerView.ItemDecoration {

    private final int FIRST_ITEM_TOP_OFFSET = 55;
    private final int mVerticalSpaceHeight;
    private final float mDensity;

    public TalkRoomItemDecoration(int verticalSpaceHeight, float density) {
        mVerticalSpaceHeight = verticalSpaceHeight;
        mDensity = density;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = (int) (FIRST_ITEM_TOP_OFFSET * mDensity);
        }

        if (state.getItemCount() - 1 == parent.getChildAdapterPosition(view)) {
            outRect.bottom = 0;
        } else {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }
}
